<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('mod', function($attribute, $value, $parameters, $validator) {
            if($value%$parameters[0] == 0){
                return true;
            }
                return false;
        });

        Validator::replacer('mod', function($message, $attribute, $rule, $parameters) {
            $mod = $parameters[0];

            return str_replace(':mod', $mod, $message);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
