<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use App\User;
use App\AuditTrail;

use Auth;
use DB;


class UserController extends Controller
{
    public $data;

    public function __construct(){
        $this->data = array();
        $this->data['class'] = "master";
        $this->data['subclass'] = "user";
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $keyword = !empty($request->keyword)?$request->keyword:"";
        $user = User::orderBy("name","asc");

        if($keyword != ""){
            $user = $user->where(function($q) use ($keyword){
                $q->where("name", "LIKE", "%".$keyword."%")
                  ->orWhere("email", "LIKE", "%".$keyword."%");
            });
        }

        $this->data['keyword']=$keyword;
        $this->data['users']=$user->paginate(10);

        //return $user->where('is_admin',1)->paginate(10);
        return view('user', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $user = Auth::user();
        $this->validate($request,[
                        "name" => "required",
                        "email" => "required|email|unique:users",
                        "password" => "required|min:6",
                        "address" => "required",
                        "phone_number" => "required"]);

        DB::beginTransaction();
        try{
            $data = new User();
            $data->name = $request->name;
            $data->email = $request->email;
            $data->password = bcrypt($request->password);
            $data->address = $request->address;
       

            $data->save();
    
            //save to audittrail
            $audit = new AuditTrail();
            $audit->type = "users";
            $audit->ip_address = $_SERVER['REMOTE_ADDR'];
            $audit->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $audit->action = "New User";
            $audit->data = json_encode($data->toArray());
            $audit->email = $user->email;

            $audit->save();

        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with("msgerr", "Terjadi kesalahan pada sistem, silahkan hubungi administrator anda.")->withInput();
        }

        DB::commit();

        return redirect()->back()->with("msg", "Admin baru telah ditambahkan");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $user = User::findOrFail($id);

        return response()->json($user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = Auth::user();
    
        $data = User::findOrFail($id);
        $data_before = json_encode($data->toArray());

        $this->validate($request,[
                        "name" => "required",
                        "email" => ['required','email',
                                    Rule::unique('users')->ignore($data->id),
                                  ],
                        "address" => "required",
                        "phone_number" => "required",
                        
                        ]);

        DB::beginTransaction();
        try{

            if($request->password){
                $data->password = $request->password;
            }
            $data->name = $request->name;
            $data->email = $request->email;
            $data->phone_number = $request->phone_number;
            $data->address = $request->address;

            $data->save();
    
            //save to audittrail
            $audit = new AuditTrail();
            $audit->type = "users";
            $audit->ip_address = $_SERVER['REMOTE_ADDR'];
            $audit->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $audit->action = "Update User";
            $audit->data_before = $data_before;
            $audit->data = json_encode($data->toArray());
            $audit->email = $user->email;

            $audit->save();

        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with("msgerr", "Terjadi kesalahan pada sistem, silahkan hubungi administrator anda.".$e->getMessage());
        }

        DB::commit();
        

        return redirect()->back()->with("msg", "Data admin berhasil diubah");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $auth = Auth::user();
        DB::beginTransaction();
        try{
            $user = User::findOrFail($id);
            $user->delete();
    
            //save to audittrail
            $audit = new AuditTrail();
            $audit->type = "users";
            $audit->ip_address = $_SERVER['REMOTE_ADDR'];
            $audit->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $audit->action = "Delete User";
            $audit->data = json_encode($user->toArray());
            $audit->email = $auth->email;

            $audit->save();

        }catch(\Exception $e){
            DB::rollback();
            return redirect()->back()->with("msgerr", "Terjadi kesalahan pada sistem, silahkan hubungi administrator anda.");
        }

        DB::commit();
        

        return redirect()->back()->with("msg", "Admin dinonaktifkan");
    }
}
