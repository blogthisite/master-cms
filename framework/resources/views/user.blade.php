@extends('layouts.app')

@section('title')
  Users Management
@endsection

@section('css')
<link rel="stylesheet" href="{{ url('/') }}/assets/plugins/select2/select2.min.css">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ url('/') }}/assets/plugins/daterangepicker/daterangepicker.css">
@endsection

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          @if (count($errors) > 0)
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
          @endif

          @if (session('msg'))
            <div class="alert alert-success">
              <p>{{ session('msg') }}</p>      
            </div>
          @endif
          
          @if (session('msgerr'))
            <div class="alert alert-danger">
              <p>{{ session('msgerr') }}</p>      
            </div>
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-md-12">
          <div class="row">
            <div class="col-xs-4">
              <div class="box">
                <div class="box-header">
                  <i class="fa fa-search"></i>

                  <h3 class="box-title">Filter</h3>

                  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                  </div>
                </div>
                <form role="form" enctype="multipart/form-data" action="{{ url('users') }}" method="get">
                <div class="box-body">
                  <div class="row">
                    <div class="col-xs-12 col-md-12">
                        <div class="form-group">
                          <label>Keyword :</label>
                          <input class="form-control" name="keyword" placeholder="Enter name, email, phone number" value="{{ $keyword }}">
                        </div>
                    </div>
                  </div>
                </div>
                <!-- /.chat -->
                <div class="box-footer">
                  <button class="btn btn-default pull-right btn-sm">Filter</button>
                  <a href="{{ url('/') }}/users" class="btn btn-default btn-sm">Clear</a>
                </div>
                </form>
              </div>
            </div>
          </div>  
        </div>
        <div class="col-xs-12 col-md-12">
          <div class="box">
            <div class="box-header">
              <i class="fa fa-mobile"></i>

              <h3 class="box-title">Users Management</h3>

              <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
                <!-- Button trigger modal -->
               <!--  <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#new-modal">
                  <i class="fa fa-plus"> New</i>
                </button> -->
              </div>
            </div>
            <div class="box-body">
              <table class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Phone Number</th>
                  <th>Address</th>
                  <th>Created Date</th>
                  <th><i class="fa fa-ellipsis-h"></i></th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $val)
                <tr>
                  <td>{{ $val->name }}</td>
                  <td>{{ $val->email }}</td>
                  <td>{{ $val->phone_number }}</td>
                  <td>{{ $val->address }}</td>
                  <td>{{ $val->created_at }}</td>
                  <td>
                    <button class="btn btn-success btn-xs edit-user" data-id="{{ $val->id }}""><i class="fa fa-pencil"></i></button>
                    <a href="{{ url('/') }}/user/delete/{{ $val->id }}" class="btn btn-danger btn-xs" data-id="{{ $val->id }}" onclick="event.preventDefault();document.getElementById('form-delete-{{ $val->id }}').submit();">
                      <i class="fa fa-trash"></i>
                    </a>
                    <form role="form" action="{{ url('/user/delete/'.$val->id) }}" method="post" id="form-delete-{{ $val->id }}">
                      {{ method_field('delete') }}
                      {{ csrf_field() }}
                    </form>
                  </td>
                </tr>
                @endforeach
                </tbody>
              </table>
            </div>
            <!-- /.chat -->
            <div class="box-footer">
              {{ $users->appends(["keyword"=>@$keyword])->links() }}
            </div>
          </div>
        </div> 
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  </div>

  <div class="modal fade" tabindex="-1" role="dialog" id="new-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form role="form" enctype="multipart/form-data" action="{{ url('user') }}" method="post">
        <div class="modal-body">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Name :</label>
                  <input class="form-control" name="name">
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Email :</label>
                  <input class="form-control" name="email" >
                </div>
            </div>
          </div>
           <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Password :</label>
                  <input class="form-control" name="email" >
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Address :</label>
                  <textarea class="form-control" name="address"></textarea>
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Phone Number:</label>
                  <input class="form-control" name="phone_number">
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-default pull-right">Save</button>
        </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->

  <div class="modal fade" tabindex="-1" role="dialog" id="edit-modal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <form role="form" enctype="multipart/form-data" action="" method="post" id="edit-form">
        <div class="modal-body">
          {{ method_field('PUT') }}
          {{ csrf_field() }}
          <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Name :</label>
                  <input class="form-control" name="name" id="edit-name">
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Email :</label>
                  <input class="form-control" name="email" id="edit-email" >
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Address :</label>
                  <textarea class="form-control" name="address" id="edit-address"></textarea>
                </div>
            </div>
          </div>
          <div class="row">
            <div class="col-xs-12">
                <div class="form-group">
                  <label>Phone Number:</label>
                  <input class="form-control" name="phone_number" id="edit-phone_number" >
                </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button class="btn btn-default pull-right">Save Change</button>
        </div>
        </form>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endsection

@section('javascript')
<script src="{{ url('/') }}/assets/plugins/select2/select2.min.js"></script>
<script src="{{ url('/') }}/assets/js/user.js"></script>

@endsection