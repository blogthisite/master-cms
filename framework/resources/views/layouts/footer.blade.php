 <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://corocot.com">Corocot Digital Creative</a>.</strong> All rights
    reserved.
  </footer>